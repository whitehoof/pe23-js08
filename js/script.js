/* ТЕОРЕТИЧНІ:

1. Опишіть своїми словами що таке Document Object Model (DOM)

Це логічна деревовидна структура веб-документу, яка визначає, як можна взаємодіяти з елементами цього веб-документу (читаючи, змінюючи, видаляючи їх). Це наче програмний інтерфейс між веб-сторінкою та розробником.





2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

innerHTML - це властивість Element. innerText — це властивість HTMLElement (це нащадок Element). Тобто, innerHTML та innerText знаходяться в різних "наборах" властивостей.

Також, вони повертають різні результати:

innerHTML повертає всю вкладену в нього html-розмітку. Тобто, innerHTML ніби повертає source code з середини елементу, як його видно на стороні фронтенду (наприклад, в браузері).
innerText повертає весь текст, що міститься в елементі, і текст з усіх його дочірніх елементів, ігноруючи html-розмітку.





3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

3.1. Кращого нема, вибір способу залежить від задачі. Методи querySelectorAll та getElementsByName повертають нам NodeList, який можна перебирати forEach, а щоб перебрати методом forEach HTMLколекцію (яку нам дають getElementsByClassName та getElementsByTagName), доведеться розпаковувати її спредом в простий архів або застосувати цикл for. Але ж у списку NodeList також присутні ще й текстові ноди, які нас, мабуть, далеко не завжди цікавлять при роботі з елементами DOM, тож їх доведеться відфільтровувати. Нема ідеального методу.

Для перебору кількох елементів краще підійдуть методи, які повертають кілька елементів. Ось як можна звернутися до одного конкретного елемента сторінки:

document.getElementById(ID_ЕЛЕМЕНТА)

document.getElementsByClassName(НАЗВА_CSS_КЛАСУ)[порядковий_номер_елемента_серед_знайдених]

document.getElementsByName(ЗНАЧЕННЯ_АТРИБУТУ_NAME_ЕЛЕМЕНТІВ)[порядковий_номер_елемента_серед_знайдених]

document.getElementsByTagName(НАЗВА_HTML_ТЕГУ)[порядковий_номер_елемента_серед_знайдених]

document.querySelector(CSS_СЕЛЕКТОРИ) // буде повернуто перший елемент, що відповідає зазначеним селекторам

document.querySelectorAll(CSS_СЕЛЕКТОРИ)[порядковий_номер_елемента_серед_знайдених]
*/


// ПРАКТИЧНЕ ЗАВДАННЯ:

// 1 покраска в червоний
document.querySelectorAll("p").forEach(el => el.style.backgroundColor = "#ff0000");



// 2 #optionsList

// Знайти елемент із id="optionsList". Вивести у консоль.
console.log(document.getElementById("optionsList"));

// Знайти батьківський елемент та вивести в консоль.
console.log(document.getElementById("optionsList").parentElement);

// вивести в консоль назви та тип дочірніх нод

// оскільки тип ноди виводиться як цифра і це зовсім не інформативно, можна з документації створити обʼєкт з усіма наявними на дату виконання ДЗ типами нод:
// цей обʼєкт з назвами може застаріти з часом, тому це не найкращий спосіб, але до кінця 2023 року так буде норм:
const NODE_TYPES = {
	"1" : "ELEMENT_NODE",
	"2" : "ATTRIBUTE_NODE",
	"3" : "TEXT_NODE",
	"4" : "CDATA_SECTION_NOD",
	"7" : "PROCESSING_INSTRUCTION_NODE",
	"8" : "COMMENT_NODE",
	"9" : "DOCUMENT_NODE",
	"10": "DOCUMENT_TYPE_NODE",
	"11": "DOCUMENT_FRAGMENT_NODE",
}

// Виводимо назву та тип кожної ноди:
document.getElementById("optionsList").childNodes.forEach(kid => console.log(`name: ${kid.nodeName}, type: ${NODE_TYPES[kid.nodeType]}`));



// 3 Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// "This is a paragraph"
// Ми не знаємо, скільки елементів документу мають клас testParagraph — тому треба перебирати всі можливі:
document.querySelectorAll(".testParagraph").forEach(p => p.innerHTML = "This is a paragraph");



// 4 та 5 Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль
let mainHeaderKids = document.querySelector(".main-header").children;
console.log(mainHeaderKids);

// Кожному з елементів присвоїти новий клас nav-item.
// оскільки HTMLCollection не має методу forEach, спробуємо розпакувати колекцію спред-оператором в порожній літеральний архів, щоб вже цей наповнений елементами архів перебрати методом forEach:
[...mainHeaderKids].forEach(kid => kid.classList.add("nav-item"));



// 6 Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
// Для цього краще отримати NodeList:
document.querySelectorAll(".section-title").forEach(node => node.classList.remove("section-title"));
